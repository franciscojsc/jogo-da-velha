package br.com.franciscochaves.jogodavelha;

/**
 * 
 * @author Francisco
 *
 */
public class Jogador {

	private int pontos = 0;
	private String nome, simbolo;

	public Jogador(String nome, String simbolo) {
		this.nome = nome;
		this.simbolo = simbolo;
	}

	/**
	 * @return the pontos
	 */
	public int getPontos() {
		return pontos;
	}

	/**
	 * @param pontos the pontos to set
	 *            
	 */
	public void setPontos(int pontos) {
		this.pontos = pontos;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 *            
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the simbolo
	 */
	public String getSimbolo() {
		return simbolo;
	}

	/**
	 * @param simbolo the simbolo to set
	 *           
	 */
	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}
}
