
package br.com.franciscochaves.jogodavelha;

import java.awt.Color;
import javax.swing.JOptionPane;

/**
 *
 * @author Francisco
 */
@SuppressWarnings("serial")
public class Window extends javax.swing.JFrame {

	private Jogador jogador1;
	private Jogador jogador2;

	private int jogador = 0;
	public static int cont = 0;

	public Window() {
		initComponents();
		jogador1 = new Jogador(JOptionPane.showInputDialog(null, "Digite o nome do jogador 1"),
				JOptionPane.showInputDialog(null, "Digite o simbolo de jogador 1"));
		jogador2 = new Jogador(JOptionPane.showInputDialog(null, "Digite o nome do jogador 2"),
				JOptionPane.showInputDialog(null, "Digite o simbolo de jogador 2"));
	}

	private void initComponents() {

		jButton1 = new javax.swing.JButton();
		jButton2 = new javax.swing.JButton();
		jButton3 = new javax.swing.JButton();
		jButton4 = new javax.swing.JButton();
		jButton5 = new javax.swing.JButton();
		jButton6 = new javax.swing.JButton();
		jButton7 = new javax.swing.JButton();
		jButton8 = new javax.swing.JButton();
		jButton9 = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		getContentPane().setLayout(new java.awt.GridLayout(3, 3));

		jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});
		getContentPane().add(jButton1);

		jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton2ActionPerformed(evt);
			}
		});
		getContentPane().add(jButton2);

		jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton3ActionPerformed(evt);
			}
		});
		getContentPane().add(jButton3);

		jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton4ActionPerformed(evt);
			}
		});
		getContentPane().add(jButton4);

		jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton5ActionPerformed(evt);
			}
		});
		getContentPane().add(jButton5);

		jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton6ActionPerformed(evt);
			}
		});
		getContentPane().add(jButton6);

		jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton7ActionPerformed(evt);
			}
		});
		getContentPane().add(jButton7);

		jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton8ActionPerformed(evt);
			}
		});
		getContentPane().add(jButton8);

		jButton9.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton9ActionPerformed(evt);
			}
		});
		getContentPane().add(jButton9);

		setBounds(0, 0, 423, 330);
	}

	private void alterarSimbolo(javax.swing.JButton botao) {

		if (botao.getText().equals("")) {
			if (jogador == 0) {
				botao.setText(jogador1.getSimbolo());
				botao.setForeground(Color.RED);
				jogador = 1;
				cont++;
			} else {
				botao.setText(jogador2.getSimbolo());
				botao.setForeground(Color.BLUE);
				jogador = 0;
				cont++;
			}
		}
	}

	private void mostraTabelaAtualJogador(Jogador j) {
		JOptionPane.showMessageDialog(null, "Vencedor: " + j.getNome() + ", " + j.getSimbolo());
		j.setPontos(j.getPontos() + 1);
		novaPartida();
		JOptionPane.showMessageDialog(null, "Pontos da partidas: " + jogador1.getNome() + ": " + jogador1.getPontos() + " --- "
				+ jogador2.getNome() + ": " + jogador2.getPontos());
	}

	private void verificarVitoria() {
		// verificar na horizontal
		if (jButton1.getText().equals(jogador1.getSimbolo()) && jButton2.getText().equals(jogador1.getSimbolo())
				&& jButton3.getText().equals(jogador1.getSimbolo())) {
			mostraTabelaAtualJogador(jogador1);
		} else if (jButton1.getText().equals(jogador2.getSimbolo()) && jButton2.getText().equals(jogador2.getSimbolo())
				&& jButton3.getText().equals(jogador2.getSimbolo())) {
			mostraTabelaAtualJogador(jogador2);
		} else if (jButton4.getText().equals(jogador1.getSimbolo()) && jButton5.getText().equals(jogador1.getSimbolo())
				&& jButton6.getText().equals(jogador1.getSimbolo())) {
			mostraTabelaAtualJogador(jogador1);
		} else if (jButton4.getText().equals(jogador2.getSimbolo()) && jButton5.getText().equals(jogador2.getSimbolo())
				&& jButton6.getText().equals(jogador2.getSimbolo())) {
			mostraTabelaAtualJogador(jogador2);
		} else if (jButton7.getText().equals(jogador1.getSimbolo()) && jButton8.getText().equals(jogador1.getSimbolo())
				&& jButton9.getText().equals(jogador1.getSimbolo())) {
			mostraTabelaAtualJogador(jogador1);
		} else if (jButton7.getText().equals(jogador2.getSimbolo()) && jButton8.getText().equals(jogador2.getSimbolo())
				&& jButton9.getText().equals(jogador2.getSimbolo())) {
			mostraTabelaAtualJogador(jogador2);
		}
		// verificar na vertical
		if (jButton1.getText().equals(jogador1.getSimbolo()) && jButton4.getText().equals(jogador1.getSimbolo())
				&& jButton7.getText().equals(jogador1.getSimbolo())) {
			mostraTabelaAtualJogador(jogador1);
		} else if (jButton1.getText().equals(jogador2.getSimbolo()) && jButton4.getText().equals(jogador2.getSimbolo())
				&& jButton7.getText().equals(jogador2.getSimbolo())) {
			mostraTabelaAtualJogador(jogador2);
		} else if (jButton2.getText().equals(jogador1.getSimbolo()) && jButton5.getText().equals(jogador1.getSimbolo())
				&& jButton8.getText().equals(jogador1.getSimbolo())) {
			mostraTabelaAtualJogador(jogador1);
		} else if (jButton2.getText().equals(jogador2.getSimbolo()) && jButton5.getText().equals(jogador2.getSimbolo())
				&& jButton8.getText().equals(jogador2.getSimbolo())) {
			mostraTabelaAtualJogador(jogador2);
		} else if (jButton3.getText().equals(jogador1.getSimbolo()) && jButton6.getText().equals(jogador1.getSimbolo())
				&& jButton9.getText().equals(jogador1.getSimbolo())) {
			mostraTabelaAtualJogador(jogador1);
		} else if (jButton3.getText().equals(jogador2.getSimbolo()) && jButton6.getText().equals(jogador2.getSimbolo())
				&& jButton9.getText().equals(jogador2.getSimbolo())) {
			mostraTabelaAtualJogador(jogador2);
		}
		// verificar na diagonal
		if (jButton1.getText().equals(jogador1.getSimbolo()) && jButton5.getText().equals(jogador1.getSimbolo())
				&& jButton9.getText().equals(jogador1.getSimbolo())) {
			mostraTabelaAtualJogador(jogador1);
		} else if (jButton1.getText().equals(jogador2.getSimbolo()) && jButton5.getText().equals(jogador2.getSimbolo())
				&& jButton9.getText().equals(jogador2.getSimbolo())) {
			mostraTabelaAtualJogador(jogador2);
		} else if (jButton3.getText().equals(jogador1.getSimbolo()) && jButton5.getText().equals(jogador1.getSimbolo())
				&& jButton7.getText().equals(jogador1.getSimbolo())) {
			mostraTabelaAtualJogador(jogador1);
		} else if (jButton3.getText().equals(jogador2.getSimbolo()) && jButton5.getText().equals(jogador2.getSimbolo())
				&& jButton7.getText().equals(jogador2.getSimbolo())) {
			mostraTabelaAtualJogador(jogador2);
		}
	}

	private void analisaVencedor() {
		if (cont >= 5) {
			verificarVitoria();
		}
		if (cont == 9) {
			JOptionPane.showMessageDialog(null, "Empate");
			novaPartida();
		}
	}

	private void novaPartida() {
		jButton1.setText("");
		jButton2.setText("");
		jButton3.setText("");
		jButton4.setText("");
		jButton5.setText("");
		jButton6.setText("");
		jButton7.setText("");
		jButton8.setText("");
		jButton9.setText("");

		cont = 0;
	}

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
		alterarSimbolo(jButton1);
		analisaVencedor();
	}

	private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {
		alterarSimbolo(jButton7);
		analisaVencedor();
	}

	private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
		alterarSimbolo(jButton2);
		analisaVencedor();
	}

	private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {
		alterarSimbolo(jButton3);
		analisaVencedor();
	}

	private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {
		alterarSimbolo(jButton4);
		analisaVencedor();
	}

	private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {
		alterarSimbolo(jButton5);
		analisaVencedor();
	}

	private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {
		alterarSimbolo(jButton6);
		analisaVencedor();
	}

	private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {
		alterarSimbolo(jButton8);
		analisaVencedor();
	}

	private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {
		alterarSimbolo(jButton9);
		analisaVencedor();
	}

	public static void main(String args[]) {

		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(Window.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(Window.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(Window.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(Window.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new Window().setVisible(true);
			}
		});
	}

	private javax.swing.JButton jButton1;
	private javax.swing.JButton jButton2;
	private javax.swing.JButton jButton3;
	private javax.swing.JButton jButton4;
	private javax.swing.JButton jButton5;
	private javax.swing.JButton jButton6;
	private javax.swing.JButton jButton7;
	private javax.swing.JButton jButton8;
	private javax.swing.JButton jButton9;

}
